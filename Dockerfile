FROM centos:7

# Установка Python и pip
RUN yum install -y python3 python3-pip

# Создание директории для приложения
RUN mkdir /python_api
WORKDIR /python_api

# Копирование requirements.txt и установка зависимостей
COPY requiremets.txt requirements.txt
RUN pip3 install -r requirements.txt

# Копирование скрипта приложения
COPY app.py .

# Точка вызова
CMD ["python3", "app.py"]